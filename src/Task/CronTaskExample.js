'use strict';
const CronTaskInterface = require('./CronTaskInterface');

/**
 * @class CronTaskExample
 * @type CronTaskInterface
 */
class CronTaskExample extends CronTaskInterface {

    /**
     *
     * @param {CronOptionInterface} options
     * @param {string} uuid
     */
    constructor(options, uuid) {
        super();
        /**
         *
         * @type {CronOptionInterface}
         * @private
         */
        this._options = options;
        this._uuid = uuid;
    }

    /**
     *
     * @return {CronOptionInterface}
     */
    getOptions() {
        return this._options;
    }

    /**
     * @return {string}
     */
    getTime() {
        return '* * * * *';
    }

    /**
     * @return {string}
     */
    getUUID() {
        return this._uuid;
    }

    /**
     * @return {Promise}
     */
    run() {
        return new Promise(resolve => {
            console.log('Running: ' + this.getUUID());
            console.log('Done.');
            return resolve();
        });
    }
}

module.exports = CronTaskExample;
