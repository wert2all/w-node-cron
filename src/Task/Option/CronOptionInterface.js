'use strict';
const CronImplementationError = require('../../Errors/CronImplementationError');

/**
 * @class CronOptionInterface
 * @type CronOptionInterface
 * @abstract
 */
class CronOptionInterface {
    /**
     * @return {boolean}
     * @abstract
     */
    isActive() {
        throw  new CronImplementationError(this, 'isActive');
    }

    /**
     * @return {boolean}
     * @abstract
     */
    canExecutedRunning() {
        throw  new CronImplementationError(this, 'canExecutedRunning');
    }

    /**
     * @return {boolean}
     * @abstract
     */
    canErrorRunning() {
        throw  new CronImplementationError(this, 'canErrorRunning');
    }
}

module.exports = CronOptionInterface;
