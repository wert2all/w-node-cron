'use strict';
const CronOptionInterface = require('./CronOptionInterface');

/**
 * @class CronOption
 * @type CronOptionInterface
 */
class CronOption extends CronOptionInterface {

    /**
     *
     * @param {{}} options
     */
    constructor(options) {
        super();
        this._options = Object.assign({
            isActive: false,
            canErrorRunning: false,
            canExecutedRunning: false
        }, options);
    }

    /**
     * @return {boolean}
     */
    isActive() {
        return this._options.isActive;
    }

    /**
     *
     * @return {boolean}
     */
    canErrorRunning() {
        return this._options.canErrorRunning;
    }

    /**
     *
     * @return {boolean}
     */
    canExecutedRunning() {
        return this._options.canExecutedRunning;
    }
}

module.exports = CronOption;
