'use strict';
const CronImplementationError = require('../Errors/CronImplementationError');

/**
 * @class CronTaskInterface
 * @type CronTaskInterface
 * @abstract
 */
class CronTaskInterface {
    /**
     * @return {CronOptionInterface}
     * @abstract
     */
    getOptions() {
        throw new CronImplementationError(this, 'getOptions');
    }

    /**
     * @return {string}
     * @abstract
     */
    getTime() {
        throw new CronImplementationError(this, 'getTime');
    }

    /**
     * @return {string}
     * @abstract
     */
    getUUID() {
        throw new CronImplementationError(this, 'getUUID');
    }

    /**
     * @return {Promise}
     * @abstract
     */
    run() {
        throw new CronImplementationError(this, 'run');
    }
}

module.exports = CronTaskInterface;
