'use strict';

const ThrowableImplementationError = require('w-node-implementation-error');

/**
 * @class CronImplementationError
 * @type ThrowableImplementationError
 */
class CronImplementationError extends ThrowableImplementationError {

}

module.exports = CronImplementationError;
