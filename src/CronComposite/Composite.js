'use strict';
const CompositeInterface = require('./CompositeInterface');

/**
 * @class Composite
 * @type CompositeInterface
 */
class Composite extends CompositeInterface {
    /**
     *
     * @param {Array<CronTaskInterface>} crones
     */
    constructor(crones) {
        super();
        /**
         *
         * @type {Array<CronTaskInterface>}
         * @private
         */
        this._crons = Array.isArray(crones) ? crones : [];
    }

    /**
     * @return {Array<CronTaskInterface>}
     */
    fetch() {
        return this._crons;
    }
}

module.exports = Composite;
