'use strict';
const CronImplementationError = require('../Errors/CronImplementationError');

/**
 * @class CompositeInterface
 * @type CompositeInterface
 * @abstract
 */
class CompositeInterface {
    /**
     * @return {Array<CronTaskInterface>}
     * @abstract
     */
    fetch() {
        throw new CronImplementationError(this, 'fetch');
    }
}

module.exports = CompositeInterface;
