'use strict';
const CronRunner = require('./CronRunner');
const cronObject = require('node-cron');

/**
 * @class CronManager
 * @type CronManager
 */
class CronManager {
    /**
     *
     * @param {CompositeInterface} cronComposite
     * @param {CronStatusGetterInterface} cronStatusSetter
     * @param {CronStatusChangerInterface} cronStatusChanger
     */
    constructor(cronComposite, cronStatusSetter, cronStatusChanger) {
        /**
         *
         * @type {CompositeInterface}
         * @private
         */
        this._crons = cronComposite;
        /**
         *
         * @type {Array<ScheduledTask>}
         * @private
         */
        this._tasks = [];
        /**
         *
         * @type {CronStatusGetterInterface}
         * @private
         */
        this._cronStatusSetter = cronStatusSetter;
        /**
         *
         * @type {CronStatusChangerInterface}
         * @private
         */
        this._cronStatusChanger = cronStatusChanger;
    }

    run() {
        this._scheduleTasks();
        this._tasks
            .map(task => task.start);
    }

    _scheduleTasks() {
        this._tasks = this._crons
            .fetch()
            .map(cron => {
                /**
                 *
                 * @type {CronOptionInterface}
                 */
                const options = cron.getOptions();
                if (options.isActive()) {
                    const cr = new CronRunner(
                        cron,
                        this._cronStatusSetter,
                        this._cronStatusChanger
                    );
                    return cronObject
                        .schedule(cron.getTime(), () => cr.run(), {});
                } else {
                    return null;
                }
            })
            .filter(cron => !!cron);
    }
}

module.exports = CronManager;
