'use strict';

/**
 * @class CronStatusItem
 * @type CronStatusItem
 */
class CronStatusItem {
    /**
     *
     * @param {boolean} isRunning
     * @param {boolean} isError
     */
    constructor(isRunning, isError) {
        /**
         * @type {boolean}
         * @private
         */
        this._isRunning = isRunning;
        /**
         * @type {boolean}
         * @private
         */
        this._isError = isError;
    }

    /**
     * @return {boolean}
     */
    isRunning() {
        return this._isRunning;
    }

    /**
     *
     * @return {boolean}
     */
    isError() {
        return this._isError;
    }
}

module.exports = CronStatusItem;
