'use strict';
const CronImplementationError = require('./../Errors/CronImplementationError');

/**
 * @class CronStatusGetterInterface
 * @type CronStatusGetterInterface
 * @interface
 */
class CronStatusGetterInterface {
    /**
     * @return {Promise<CronStatusItem>}
     * @param {CronTaskInterface} cron
     * @abstract
     */
    // eslint-disable-next-line no-unused-vars
    getLastStatus(cron) {
        throw new CronImplementationError(this, 'getLastStatus');
    }
}

module.exports = CronStatusGetterInterface;
