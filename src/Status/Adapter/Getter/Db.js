'use strict';
const Op = require('sequelize').Op;
const DBRepository = require('w-node-db-repository');
const DBLimitDefault = require('w-node-db-repository').LimitDefault;
const DBOrderDefault = require('w-node-db-repository').OrderDefault;
const DBFilterDefault = require('w-node-db-repository').FilterDefault;

const CronStatusItem = require('./../../CronStatusItem');

const CronStatusGetterInterface = require('../../CronStatusGetterInterface');

/**
 * @class CronStatusGetterAdapterDB
 * @type CronStatusInterface
 */
class CronStatusGetterAdapterDB extends CronStatusGetterInterface {
    /**
     *
     * @param {DBModelInterface} cronModel
     */
    constructor(cronModel) {
        super();
        /**
         *
         * @type {DBModelInterface}
         * @private
         */
        this._cronModel = cronModel;
        /**
         *
         * @type {DBRepository}
         * @private
         */
        this._repository = new DBRepository(this._cronModel);
        this._repository
            .setLimit(new DBLimitDefault(0, 1))
            .setOrders([
                new DBOrderDefault(
                    this._cronModel
                        .getModelDefinition()
                        .getPrimaryColumnName()
                )
            ]);
    }

    /**
     * @return {Promise<CronStatusItem>}
     * @param {CronTaskInterface} cron
     */
    getLastStatus(cron) {
        return this._repository
            .addFilter(new DBFilterDefault('uuid', Op.eq, cron.getUUID()))
            .find()
            .then(cronStatus => {
                if (cronStatus[0]) {
                    cronStatus = cronStatus[0];
                } else {
                    cronStatus = null;
                }
                return this._createLastStatus(cronStatus);
            });
    }

    /**
     *
     * @param {EMEntity} cronStatus
     * @return {CronStatusItem}
     * @private
     */
    _createLastStatus(cronStatus = null) {
        let isRunning = false;
        let isError = false;
        if (cronStatus !== null) {
            const statusValue = cronStatus.getData().getData('status');
            if (statusValue === 'progress') {
                isRunning = true;
            }
            if (statusValue === 'error') {
                isError = true;
            }
        }

        return new CronStatusItem(isRunning, isError);
    }
}

module.exports = CronStatusGetterAdapterDB;
