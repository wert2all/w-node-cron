'use strict';
const CronStatusGetterInterface = require('../../CronStatusGetterInterface');
const CronStatusItem = require('./../../CronStatusItem');

/**
 * @class CronStatusGetterAdapterFake
 * @type CronStatusGetterInterface
 */
class CronStatusGetterAdapterFake extends CronStatusGetterInterface {

    /**
     *
     * @param {{isRunning:<boolean>, isError:<boolean>}} statusOptions
     */
    constructor(statusOptions) {
        super();
        /**
         *
         * @type {{isRunning:<boolean>, isError:<boolean>}}
         * @private
         */
        this._statusOptions = Object.assign({
            isRunning: false,
            isError: false
        }, statusOptions);
    }

    /**
     * @return {Promise<CronStatusItem>}
     * @param {CronTaskInterface} cron
     */
    // eslint-disable-next-line no-unused-vars
    getLastStatus(cron) {
        return new Promise(resolve => {
            resolve(
                new CronStatusItem(
                    this._statusOptions.isRunning,
                    this._statusOptions.isError
                )
            );
        });
    }
}

module.exports = CronStatusGetterAdapterFake;
