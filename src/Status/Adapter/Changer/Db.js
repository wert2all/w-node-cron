'use strict';

const CronStatusChangerInterface = require('../../CronStatusChangerInterface');

/**
 * @class CronStatusChangerAdapterDB
 * @type CronStatusChangerInterface
 */
class CronStatusChangerAdapterDB extends CronStatusChangerInterface {
    /**
     *
     * @param {DBModelInterface} cronModel
     * @param {EMEntityManager} entityManager
     */
    constructor(cronModel, entityManager) {
        super();
        /**
         *
         * @type {EMEntityManager}
         * @private
         */
        this._entityManager = entityManager;
        /**
         *
         * @type {DBModelInterface}
         * @private
         */
        this._cronModel = cronModel;

        /**
         *
         * @type {EMEntity|null}
         * @private
         */
        this._currentStatus = null;
    }

    /**
     *
     * @param {string} errorMessage
     * @param {CronTaskInterface} cron
     * @return {Promise}
     */
    setError(errorMessage, cron) {
        return this._changeStatus(cron, 'error', errorMessage);
    }

    /**
     *
     * @param {CronTaskInterface} cron
     * @param {string} status
     * @param {string} errorMessage
     * @return {Promise<EMEntity>}
     * @private
     */
    _changeStatus(cron, status, errorMessage = '') {
        if (this._currentStatus === null) {
            this._currentStatus = this._createCurrentStatus(cron);
        }
        this._currentStatus
            .setValue('status', status)
            .setValue('message', errorMessage);

        return this._entityManager.save(this._currentStatus);
    }

    /**
     * @param {CronTaskInterface} cron
     * @return {Promise}
     */
    setProcess(cron) {
        return this._changeStatus(cron, 'progress');
    }

    /**
     * @param {CronTaskInterface} cron
     * @return {Promise}
     */
    setDone(cron) {
        return this._changeStatus(cron, 'done');
    }

    /**
     *
     * @param {CronTaskInterface} cron
     * @return {EMEntity}
     * @private
     */
    _createCurrentStatus(cron) {
        return this._entityManager.create(this._cronModel, {
            'uuid': cron.getUUID()
        });
    }
}

module.exports = CronStatusChangerAdapterDB;
