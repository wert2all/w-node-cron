'use strict';
const CronStatusChangerInterface = require('../../CronStatusChangerInterface');

/**
 * @class CronStatusGetterAdapterFake
 * @type CronStatusChangerInterface
 */
class CronStatusGetterAdapterFake extends CronStatusChangerInterface {

    /**
     *
     * @param {{isRunning:<boolean>, isError:<boolean>}} statusOptions
     */
    constructor(statusOptions) {
        super();
        /**
         *
         * @type {{isRunning:<boolean>, isError:<boolean>}}
         * @private
         */
        this._statusOptions = Object.assign({
            isRunning: false,
            isError: false
        }, statusOptions);
    }

    /**
     * @param {string} errorMessage
     * @param {CronTaskInterface} cron
     * @return {Promise}
     */
    // eslint-disable-next-line no-unused-vars
    setError(errorMessage, cron) {
        return new Promise(resolve => {
            console.log(errorMessage);
            resolve();
        });
    }

    /**
     * @param {CronTaskInterface} cron
     * @return {Promise}
     */
    // eslint-disable-next-line no-unused-vars
    setDone(cron) {
        return new Promise(resolve => resolve());
    }

    /**
     * @param {CronTaskInterface} cron
     * @return {Promise}
     */
    // eslint-disable-next-line no-unused-vars
    setProcess(cron) {
        return new Promise(resolve => resolve());
    }
}

module.exports = CronStatusGetterAdapterFake;
