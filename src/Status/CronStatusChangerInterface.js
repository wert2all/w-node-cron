'use strict';
const CronImplementationError = require('./../Errors/CronImplementationError');

/**
 * @class CronStatusChangerInterface
 * @type CronStatusChangerInterface
 * @interface
 */
class CronStatusChangerInterface {

    /**
     *
     * @param {string} errorMessage
     * @param {CronTaskInterface} cron
     * @return {Promise}
     * @abstract
     */
    // eslint-disable-next-line no-unused-vars
    setError(errorMessage, cron) {
        throw new CronImplementationError(this, 'setError');
    }

    /**
     * @param {CronTaskInterface} cron
     * @return {Promise}
     * @abstract
     */
    // eslint-disable-next-line no-unused-vars
    setProcess(cron) {
        throw new CronImplementationError(this, 'setProcess');
    }

    /**
     * @param {CronTaskInterface} cron
     * @return {Promise}
     * @abstract
     */
    // eslint-disable-next-line no-unused-vars
    setDone(cron) {
        throw new CronImplementationError(this, 'setDone');
    }
}

module.exports = CronStatusChangerInterface;
