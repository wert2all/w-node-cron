'use strict';

/**
 * @class CronRunner
 * @type CronRunner
 */
class CronRunner {
    /**
     *
     * @param {CronTaskInterface} cron
     * @param {CronStatusGetterInterface} cronStatusSetter
     * @param {CronStatusChangerInterface} cronStatusChanger
     */
    constructor(cron, cronStatusSetter, cronStatusChanger) {
        /**
         *
         * @type {CronTaskInterface}
         * @private
         */
        this._cron = cron;
        /**
         *
         * @type {CronStatusGetterInterface}
         * @private
         */
        this._cronStatusSetter = cronStatusSetter;
        /**
         *
         * @type {CronStatusChangerInterface}
         * @private
         */
        this._cronStatusChanger = cronStatusChanger;
    }

    run() {
        (async function (self) {
            await self._cronStatusSetter
                .getLastStatus(self._cron)
                .then(status =>
                    self._canRun(status) === true
                        ? self._run()
                        : false)
                .catch(e => self._cronStatusChanger.setError(e.message, self._cron));
        }(this));
    }

    /**
     *
     * @return {Promise}
     * @private
     */
    _run() {
        return this._cronStatusChanger
            .setProcess(this._cron)
            .then(() => this._cron.run())
            .then(() => this._cronStatusChanger.setDone(this._cron));
    }

    /**
     *
     * @param {CronStatusItem} status
     * @return {boolean}
     * @private
     */
    _canRun(status) {
        if (status.isRunning()) {
            return this._cron.getOptions().canExecutedRunning();
        }
        if (status.isError()) {
            return this._cron.getOptions().canErrorRunning();
        }
        return true;
    }
}

module.exports = CronRunner;
