'use strict';

/**
 * The entry point.
 *
 * @module CronManager
 */
module.exports = require('./src/CronManager');
module.exports.CompositeInterface = require('./src/CronComposite/CompositeInterface');
module.exports.Composite = require('./src/CronComposite/Composite');
module.exports.CronTaskInterface = require('./src/Task/CronTaskInterface');
module.exports.CronTaskExample = require('./src/Task/CronTaskExample');

module.exports.CronTaskOptionInterface =
    require('./src/Task/Option/CronOptionInterface');
module.exports.CronTaskOption = require('./src/Task/Option/CronOption');

module.exports.CronStatusChangerInterface =
    require('./src/Status/CronStatusChangerInterface');
module.exports.CronStatusGetterInterface =
    require('./src/Status/CronStatusGetterInterface');

module.exports.CronStatusAdapters = {
    Getter: {
        Fake: require('./src/Status/Adapter/Getter/Fake'),
        Db: require('./src/Status/Adapter/Getter/Db'),
    },
    Changer: {
        Fake: require('./src/Status/Adapter/Changer/Fake'),
        Db: require('./src/Status/Adapter/Changer/Db'),
    },
};
